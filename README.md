NAME
====

WebService::TMDB - A module for accessing [The Movie Database](https://themoviedb.org) data.

SYNOPSIS
========

```raku
use WebService::TMDB;

my $tmdb = WebService::TMDB.new: :access_token<1234>;

my $search = $tmdb.search_movie: query => 'legend of 1900';
say $search.results[0].title;

my $movie = $tmdb.movie: 10775, :append(['credits']);
say $movie.title;
say $movie.credits.cast[0].name;

my $find = $tmdb.find_by: ExternalSources::IMDB, :external_id<tt4540710>;
say $find.movie_results[0].title;
```

INSTALLATION
============

You need to have [Raku](https://www.raku-lang.ir/en) and [zef](https://github.com/ugexe/zef), then run:

    zef install "WebService::TMDB:auth<zef:CIAvash>"

or if you have cloned the repo:

    zef install .

TESTING
=======

    prove -ve 'raku -I.' --ext rakutest

DESCRIPTION
===========

WebService::TMDB is a module for interfacing with the [TMDB API](https://developers.themoviedb.org/3/). An access token is required to use the API.

ATTRIBUTES/METHODS
==================

### has Str $.access_token

Access token required by themoviedb.org

### has WebService::TMDB::Role::Request $.request handles('language', 'set_language')

An object for making requests to themoviedb.org

### method find_by

```raku
method find_by(
    WebService::TMDB::ExternalSources(Str) $external_source,
    Str :$external_id!
) returns WebService::TMDB::Find
```

Find movie, TV, TV season and episode by external ID

### method movie

```raku
method movie(
    Int $id,
    :@append where { ... }
) returns WebService::TMDB::Movie
```

Get a specific movie by its ID. Credits and external IDs can be appended to response.

### method movie_credits

```raku
method movie_credits(
    Int $id
) returns WebService::TMDB::Credits
```

Get credits for a movie

### method movie_external_ids

```raku
method movie_external_ids(
    Int $id
) returns WebService::TMDB::ExternalID
```

Get external IDs for a movie

### method movie_upcoming

```raku
method movie_upcoming(
    *%params
) returns WebService::TMDB::Movie::Upcoming
```

Get upcoming movies

### method movie_now_playing

```raku
method movie_now_playing(
    *%params
) returns WebService::TMDB::Movie::Upcoming
```

Get movies that are in theatres

### method search_movie

```raku
method search_movie(
    *%params
) returns WebService::TMDB::Movie::Search
```

Search for movies

### method discover_movie

```raku
method discover_movie(
    *%params
) returns WebService::TMDB::Movie::Search
```

Discover movies

### method tv

```raku
method tv(
    Int $id,
    :@append where { ... }
) returns WebService::TMDB::TV
```

Get a TV show by its ID. Credits and external IDs can be appended to response.

### method season

```raku
method season(
    Int :$tv,
    Int :$season,
    :@append where { ... }
) returns WebService::TMDB::TV::Season
```

Get a TV season. Credits can be appended to response.

### method episode

```raku
method episode(
    Int :$tv,
    Int :$season,
    Int :$episode,
    :@append where { ... }
) returns WebService::TMDB::TV::Episode
```

Get a TV episode. Credits can be appended to response.

### method tv_airing_today

```raku
method tv_airing_today(
    *%params
) returns WebService::TMDB::TV::Search
```

Get TV shows that are airing today

### method tv_on_the_air

```raku
method tv_on_the_air(
    *%params
) returns WebService::TMDB::TV::Search
```

Get TV shows that are currently on the air

### method search_tv

```raku
method search_tv(
    *%params
) returns WebService::TMDB::TV::Search
```

Search TV shows

### method discover_tv

```raku
method discover_tv(
    *%params
) returns WebService::TMDB::TV::Search
```

Discover TV shows

### method person

```raku
method person(
    Int $id,
    :@append where { ... }
) returns WebService::TMDB::Person
```

Get a person by its ID. Movie & TV credits and external IDs can be appended to response.

### method person_movie_credits

```raku
method person_movie_credits(
    Int $id
) returns WebService::TMDB::Person::MovieCredits
```

Get a person's movie credits

### method person_tv_credits

```raku
method person_tv_credits(
    Int $id
) returns WebService::TMDB::Person::TVCredits
```

Get a person's TV credits

### method person_external_ids

```raku
method person_external_ids(
    Int $id
) returns WebService::TMDB::ExternalID
```

Get a person's external IDs

### method search_person

```raku
method search_person(
    *%params
) returns WebService::TMDB::Person::Search
```

Search people

### method genre_movie_list

```raku
method genre_movie_list() returns WebService::TMDB::Genre::List
```

Get genre list for movies

### method genre_tv_list

```raku
method genre_tv_list() returns WebService::TMDB::Genre::List
```

Get genre list for TV shows

### method certification_movie_list

```raku
method certification_movie_list() returns WebService::TMDB::CertificationList
```

Get certification list for movies

### method certification_tv_list

```raku
method certification_tv_list() returns WebService::TMDB::CertificationList
```

Get certification list for TV shows

### method configuration

```raku
method configuration() returns WebService::TMDB::Configuration
```

Get themoviedb.org configuration. themoviedb.org recommends caching this data and checking for updates every few days.

ERRORS
======

`HTTP::UserAgent` module is used with exception throwing enabled. So exceptions will be thrown in case of non-existent resources, out of range values, etc. See [http://modules.raku.org/dist/HTTP::UserAgent](http://modules.raku.org/dist/HTTP::UserAgent).

When an exception of type `X::HTTP::Response` is caught, and the response received from themoviedb.org contains an error message, an attribute named `tmdb_status_message` or `tmdb_errors` will be added to the exception containing that error message.

ENVIRONMENT
===========

Some live tests will run when `NETWORK_TESTING` environment variable is set.

REPOSITORY
==========

[https://codeberg.org/CIAvash/WebService-TMDB](https://codeberg.org/CIAvash/WebService-TMDB)

BUGS
====

[https://codeberg.org/CIAvash/WebService-TMDB/issues](https://codeberg.org/CIAvash/WebService-TMDB/issues)

AUTHOR
======

Siavash Askari Nasr - https://siavash.askari-nasr.com

COPYRIGHT AND LICENSE
=====================

Copyright © Siavash Askari Nasr

WebService::TMDB is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

WebService::TMDB is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with WebService::TMDB. If not, see <http://www.gnu.org/licenses/>.

